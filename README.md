# zlx-kafka-starter

#### 介绍
此组件为springboot的开箱即用的starter,实现在特殊场景下，完成kafka消费端的热启停功能

#### 软件架构
基于springboot,kafka,nacos配置，定时任务


#### 安装教程

开箱即用引入
maven引入
````
         <dependency>
            <groupId>com.tcl.starter</groupId>
            <artifactId>tcl-starter-kafka</artifactId>
            <version>1.0.0-SNAPSHOT</version>
        </dependency>
````
yaml配置
````
 tcl:
  kafka:
    listener-data-id: kafkaStartPause.yaml #nacos监听的dataId
    listener-group: DEFAULT_GROUP #nacos监听的组
    start-pause-type: scheduled #启停触发类型
    start-listener: 2 #启动消费监听的id配置
    pause-listener: 1,2 #暂停消费的监听id配置
    scheduled-pause-cron: 0/10 * * * * ? #当为定时任务时，触发的时机

````


#### 特技


