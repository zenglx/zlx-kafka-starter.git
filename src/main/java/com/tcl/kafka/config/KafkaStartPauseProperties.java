package com.tcl.kafka.config;

import com.tcl.kafka.constant.StartPauseConstant;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;


/**
 * kafka配置参数
 * @author liangxi.zeng
 */
@ConfigurationProperties(prefix = StartPauseConstant.PREFIX)
@Data
@RefreshScope
public class KafkaStartPauseProperties {

    /**
     * 启停类型
     */
    private String startPauseType;

    /**
     * 定时cron启动消费
     */
    private String scheduledStartCron;

    /**
     * 定时cron暂停消费
     */
    private String scheduledPauseCron;

    /**
     * 暂停的监听
     */
    private String pauseListener;

    /**
     * 开启的监听id
     */
    private String startListener;

    /**
     * 监听的配置文件dataId
     */
    private String listenerDataId;


    /**
     * 监听的配置文件GROUP
     */
    private String listenerGroup = "DEFAULT_GROUP";

}
