package com.tcl.kafka.constant;

import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 * 启停类型
 * @author liangxi.zeng
 */
public class StartPauseConstant {

    /**
     * 参数配置前缀
     */
    public static final String PREFIX = "tcl.kafka";

    /**
     * 定时启动消费cron
     */
    public static final String SCHEDULED_START_CRON = "${"+PREFIX+".scheduled-start-cron:"+ScheduledTaskRegistrar.CRON_DISABLED+"}";

    /**
     * 定时暂停消费cron
     */
    public static final String SCHEDULED_PAUSE_CRON = "${"+PREFIX+".scheduled-pause-cron:"+ ScheduledTaskRegistrar.CRON_DISABLED+"}";

    /**
     * 监听的启动参数
     */
    public static final String LISTENER_START_PARAM = PREFIX+".start-listener";

    /**
     * 监听的暂停参数
     */
    public static final String LISTENER_PAUSE_PARAM = PREFIX+".pause-listener";

    /**
     *  启停类型
     */
    public static final String START_PAUSE_TYPE =  PREFIX+".start-pause-type";

    /**
     * 通过配置文件
     */
    public static final String START_PAUSE_TYPE_CONFIG = "config";

    /**
     * 通过定时任务
     */
    public static final String START_PAUSE_TYPE_SCHEDULED = "scheduled";

    /**
     * 分隔符
     */
    public static final String SPLIT=",";
}
